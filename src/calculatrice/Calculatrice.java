import java.util.Scanner;

public class Calculatrice {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Entrez le premier nombre : ");
        double nombre1 = scanner.nextDouble();

        System.out.print("Entrez le deuxième nombre : ");
        double nombre2 = scanner.nextDouble();

        System.out.println("Sélectionnez l'opération :");
        System.out.println("1. Addition");
        System.out.println("2. Soustraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Moyenne");

        int choix = scanner.nextInt();

        double resultat = 0;

        switch (choix) {
            case 1:
                resultat = nombre1 + nombre2;
                break;
            case 2:
                resultat = nombre1 - nombre2;
                break;
            case 3:
                resultat = nombre1 * nombre2;
                break;
            case 4:
                if (nombre2 != 0) {
                    resultat = nombre1 / nombre2;
                } else {
                    System.out.println("Erreur : Division par zéro");
                    return;
                }
                break;
            case 5:
                System.out.print("Entrez le troisième nombre : ");
                double nombre3 = scanner.nextDouble();
                resultat = (nombre1 + nombre2 + nombre3) / 3;
                break;
            default:
                System.out.println("Opération invalide");
                return;
        }

        System.out.println("Le résultat est : " + resultat);
    }
}